import styled from "styled-components";

const PropButton = styled.button`
    background-color:${props => props.bgColor || 'red'};
    color:${props => props.fontColor || 'white'};
    border:none;
    border-radius:5px;
    font-size: 14px;
    padding: 10px 20px;
    margin: 5px
    `;


const PrimaryButton = styled.button`
    ${props => props.primary ? 
    `background-color: #7B4CD8; color: white; ` :
    `background-color: #FF31CA; color: #fffff0; `};
    border:none;
    border-radius:5px;
    font-size: 14px;
    padding: 10px 20px;
    margin: 5px
    `    

// const PrimaryButton = styled.button`
//     background-color:${props => props.primary ? '#7B4CD8' : '#FF31CA'};
//     color:${props => props.primary ? 'white' : '#fffff0'};
//     border:none;
//     border-radius:5px;
//     font-size: 14px;
//     padding: 10px 20px;
//     margin: 5px
//     `        
export { PropButton, PrimaryButton };