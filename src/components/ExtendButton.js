import styled from "styled-components";

export const SuperButton = styled.button`
    background:#cacaca;
    color:#000000;
    border:none;
    border-radius:5px;
    font-size: 14px;
    padding: 10px 20px;
    margin: 5px
    `

export const ChildButton = styled(SuperButton)`
    background-color:${props => props.bgColor || 'red'};
    color:${props => props.fontColor || 'white'};    
    `;    
 